package com.diffeo.input;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;

// This depends on JSONObject.java, which you can get from
//   https://github.com/douglascrockford/JSON-java

/*
 * StringEscapePythonUtils provides two static methods that implement 
 * the string-escape encoding that Python v2.7.2 implements in 
 * stringobject.c as the __repr__ and DecodeEscape.  These convert between
 * a byte array and a python string literal.  
 * 
 * Such a string literal is useful for putting byte arrays into JSON strings,
 * which are really unicode character strings and thus assume an encoding for
 * the bytes.  When the input byte array is mostly latinate characters in
 * the range between the space character and char(127), then this escaping
 * does mostly nothing.
 * 
 * The method names are roughly modeled on Java's existing StringEscapeUtils, 
 * which provides tools for escaping/unescaping the Java and JavaScript 
 * flavors of string literal. 
 */
public final class StringEscapePythonUtils {
    public static String unescapePythonToString(final String escapedString) throws Exception {
        return new String(unescapePython(escapedString));
    }

    /*
    * Convert an escaped string into a byte array, i.e. convert a python string
    * literal into its corresponding byte array.
    */
    public static byte[] unescapePython(final String escapedString) throws Exception {
        // simple state machine iterates over the bytes in the escapedString and converts
        final byte[] escaped = escapedString.getBytes();
        final int length = escaped.length;
        final byte[] output = new byte[length];
        int j = 0;
        for (int i = 0; i < length; i++) {
            if (escaped[i] != '\\') {   // if its not special then just move on
                output[j++] = escaped[i];
            } else {
                switch (escaped[i + 1]) {
                    case 'x':   // deal with hex
                        if (i + 3 >= length) {  // if there's no next byte, throw incorrect encoding error
                            throw new Exception(
                                    "String incorrectly escaped, ends early with incorrect hex encoding.");
                        }
                        output[j++] = (byte) ((Character.digit(escaped[i + 2], 16) << 4) + Character.digit(escaped[i + 3], 16));
                        i += 3;
                        break;

                    case 'n':
                        output[j++] = '\n';
                        i++;
                        break;

                    case 't':
                        output[j++] = '\t';
                        i++;
                        break;

                    case 'r':
                        output[j++] = '\r';
                        i++;
                        break;

                    case '\\':
                        output[j++] = '\\';
                        ++i;
                        break;

                    case '\'':
                        output[j++] = '\'';
                        ++i;
                        break;

                    default: // invalid character
                        throw new Exception("String incorrectly escaped, invalid escaped character");
                }
            }
        }
        final byte[] unescapedTrim = new byte[j];
        System.arraycopy(output, 0, unescapedTrim, 0, j);
        // return byte array, not string. Callers can convert to string.
        return unescapedTrim;
    }

    /*
      * Converts a byte array into an escaped character string that could be used
      * as a python string literal.
      */
    public static String escapePython(final byte[] raw) throws Exception {
        final int length = raw.length;
        final StringBuilder escaped = new StringBuilder(2 * length);
        for (int i = 0; i < length; i++) {
            final byte c;
            switch (c = raw[i]) {
                case '\'':
                    escaped.append('\\').append('\'');
                    break;
                case '\\':
                    escaped.append('\\').append('\\');
                    break;
                case '\t':
                    escaped.append('\\').append('t');
                    break;
                case '\n':
                    escaped.append('\\').append('n');
                    break;
                case '\r':
                    escaped.append('\\').append('r');
                    break;
                default:
                    if (c < ' ' || c >= 0x7f) {
                        // Outside safe range, so represent as escaped hex
                        escaped.append(String.format("\\x%02x", c & 0xff));
                    } else {
                        // Just a normal character, so emit it unchanged.
                        escaped.append((char) c);
                    }
                    break;
            }
        }
        return escaped.toString();
    }

    /*
      * Tests whether unescapePython can generate an output that can be written
      * to a file on disk. Iterates over all JSON strings in a file of one
      * JSON-string per line, and unpacks the doc.body.raw for each, and writes
      * them all to a single file -- separated by newlines. This file can then be
      * compared to a similar file generated by python. If it matches exactly,
      * then we consider this test to have passed.
      */
    public static void unescapePythonFileTest(String inPath, String outPath)
            throws Exception {
        BufferedReader in = new BufferedReader(new FileReader(inPath));
        PrintWriter out = new PrintWriter(new FileWriter(outPath));
        String rawLine; // entire JSON string below (one per line in file)
        String rawBody; // string-escaped body content from inside JSON object
        JSONObject jo;
        JSONObject body;
        while (in.ready()) {
            rawLine = in.readLine();
            jo = new JSONObject(rawLine);
            body = (JSONObject) jo.get("body");
            rawBody = (String) body.get("raw");
            // write unescaped byte array to file, followed by a newline
            System.out.println(rawBody);
            out.println(new String(unescapePython(rawBody)));
        }
        out.close();
        in.close();
    }

    /*
      * Reads first JSON doc from inPath and verifies that escape(unescape(str))
      * is the identity.
      */
    public static void identityEscapePythonTest(String inPath, String outPath)
            throws Exception {
        BufferedReader in = new BufferedReader(new FileReader(inPath));
        PrintWriter out = new PrintWriter(new FileWriter(outPath));
        String rawLine; // entire JSON string below (one per line in file)
        String rawBody; // string-escaped body content from inside JSON object
        JSONObject jo;
        JSONObject body;
        rawLine = in.readLine();
        jo = new JSONObject(rawLine);
        body = (JSONObject) jo.get("body");
        rawBody = ((String) body.get("raw"));
        byte[] unescaped = unescapePython(rawBody);
        String reescaped = escapePython(unescaped);
        body.put("raw", reescaped);
        out.println(jo.toString());
        out.close();
        in.close();
        for (int i = 0; i < reescaped.length(); i++) {
            if (reescaped.charAt(i) != rawBody.charAt(i)) {
                System.out.println(i);
                System.out.println(rawBody.substring(i - 20, i + 20));
                System.out.println(unescaped.toString().substring(i - 20,
                        i + 20));
                System.out.println(reescaped.substring(i - 20, i + 20));
                System.out.println("\n");
            }
        }
    }

    /*
      * Run both the unescape test and identity test
      */
    public static void main(String[] args) throws Exception {
        String inPath = "part-01359";
        String outPath = "part-01359-transformed";
        System.err.println("Generating " + outPath);
        unescapePythonFileTest(inPath, outPath);

        String outPath2 = "part-01359-identity";
        System.err.println("Generating " + outPath2);
        identityEscapePythonTest(inPath, outPath2);

        System.out.println("success");
    }
}
