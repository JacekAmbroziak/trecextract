package com.diffeo.input;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.TextNode;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.Mongo;

import java.io.*;
import java.util.ArrayList;
import java.util.StringTokenizer;
import java.util.zip.GZIPInputStream;

/**
 * User: jacek
 * Date: 4/25/12
 * Time: 6:22 PM
 *
 * @author Jacek R. Ambroziak
 */
public class ParseTest {
    static final String CAT_OTHER = "O".intern();
    static final String CAT_PERSON = "PERSON".intern();
    static final String CAT_ORGANIZATION = "ORGANIZATION".intern();
    static final String DB_NAME = "wikidone";
    static final int MENTION_RADIUS = 3;

    public static void main(String[] args) {
        try {
            final Mongo mongo = new Mongo();
            final DB db = mongo.getDB(DB_NAME);
            db.dropDatabase();

            final DBCollection people = db.getCollection("people");

            String dirPath = "/mnt/data1/diffeo/data/wikidone";
            if (args.length > 0) {
                dirPath = args[0];
            }

            if (new File(dirPath).exists()) {
                System.out.println("dirPath = " + dirPath);
                processAllFiles(dirPath, people);
            } else {
                System.err.println("not a directory: " + dirPath);
            }
            mongo.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void processAllFiles(final String dirPath, final DBCollection people) {
        final File dir = new File(dirPath);
        if (dir.exists() || dir.isDirectory()) {
            final String[] fnames = dir.list(new FilenameFilter() {
                @Override
                public boolean accept(File dir, String name) {
                    return name.endsWith(".gz");
                }
            });
            int counter = 0;
            for (final String fname : fnames) {
                try {
                    System.out.println("fname = " + fname);
                    processFile(new File(dir, fname), people);
                    if (++counter == 200) {
                        break;
                    }
                } catch (Exception e) {
                    System.err.println(fname);
                    e.printStackTrace();
                }
            }
        } else {
            System.err.println("not a directory: " + dirPath);
        }
    }

    private static void processFile(final File gzFile, final DBCollection people) throws Exception {
        final ArrayList<String> bow = new ArrayList<String>();  // reusable in the loop below
        final Mention[] mentions = parse(gzFile);
        final int length = mentions.length;
        for (int i = 0; i < length; ++i) {
            final Mention mention = mentions[i];
            if (interestingMentionType(mention)) {
                for (int j = i - 1, count = 0; j >= 0 && count < MENTION_RADIUS; --j, ++count) {
                    bow.add(mentions[j].lemmatized);
                }
                for (int j = i + 1, count = 0; j < length && count < MENTION_RADIUS; ++j, ++count) {
                    bow.add(mentions[j].lemmatized);
                }
                people.insert(createMongoMention(mention, bow));
                bow.clear();
            }
        }
    }

    private static boolean interestingMentionType(final Mention mention) {
        final String type = mention.entityMentionType;
        return type == CAT_PERSON || type == CAT_ORGANIZATION;  // '==' ok for interned Strings
    }

    private static Mention[] parse(final File gzFile) throws Exception {
        final NerTokenBuffer nerTokenBuffer = new NerTokenBuffer();
        final GZIPInputStream gis = new GZIPInputStream(new FileInputStream(gzFile));
        final BufferedReader reader = new BufferedReader(new InputStreamReader(gis));
        final ObjectMapper mapper = new ObjectMapper();
        // reusable List of source_metadata.entities.urlname
        final ArrayList<String> urlNames = new ArrayList<String>();
        String line;
        while ((line = reader.readLine()) != null) {
            final JsonNode rootNode = mapper.readValue(line, JsonNode.class);
            urlNames.clear();   // reset
            if (rootNode.has("source_metadata")) {
                final JsonNode sourceMetadata = rootNode.get("source_metadata");
                if (sourceMetadata.has("entities")) {
                    final JsonNode entities = sourceMetadata.get("entities");
                    if (entities.isArray()) {
                        final ArrayNode entitiesAsArrayNode = (ArrayNode) entities;
                        final int size = entitiesAsArrayNode.size();
                        for (int i = 0; i < size; ++i) {
                            final JsonNode urlname = entitiesAsArrayNode.get(i).get("urlname");
                            if (urlname != null) {
                                urlNames.add(urlname.textValue());
                            }
                        }
                    }
                }
            }

            if (rootNode.has("body")) {
                final JsonNode body = rootNode.get("body");
                if (body.has("ner")) {
                    final TextNode ner = (TextNode) body.get("ner");
                    final byte[] bytes = StringEscapePythonUtils.unescapePython(ner.textValue());
                    final StringTokenizer tokens = new StringTokenizer(new String(bytes), "\n");
                    while (tokens.hasMoreTokens()) {
                        final String token = tokens.nextToken();
//                        System.out.println("token = " + token);
                        final NerToken nerToken = new NerToken(token);
//                        System.out.println("tok = " + nerToken);
                        nerTokenBuffer.add(nerToken);
                    }
                }
            }
        }

        reader.close();
        return nerTokenBuffer.getMentions();
    }

    static final class NerToken {
        final int tokenId;
        final String token;
        final String lemma;
        final String pos;
        final String entityMentionType;

        NerToken(final String line) {
            int end = line.indexOf('\t');
            tokenId = Integer.parseInt(line.substring(0, end));
            int start = end + 1;
            end = line.indexOf('\t', start);
            token = line.substring(start, end);
            end = line.indexOf('\t', start = end + 1);
            lemma = line.substring(start, end);
            end = line.indexOf('\t', start = end + 1);
            pos = line.substring(start, end).intern();
            end = line.indexOf('\t', start = end + 1);
            entityMentionType = line.substring(start, end).intern();
        }

        @Override
        public String toString() {
            return String.valueOf(tokenId) + '\t' + entityMentionType + '\t' + token;
        }
    }

    static final class Mention {
        final String entityMentionType;
        final String text;
        final String lemmatized;

        Mention(String category, String text, String lemmatized) {
            this.entityMentionType = category;
            this.text = text;
            this.lemmatized = lemmatized;
        }

        @Override
        public String toString() {
            return entityMentionType + '\t' + text;
        }
    }

    static final class NerTokenBuffer {
        int lastSeq = -1;
        String lastCategory = CAT_OTHER;
        final StringBuilder sbToken = new StringBuilder();
        final StringBuilder sbLemma = new StringBuilder();
        private final ArrayList<Mention> mentions = new ArrayList<Mention>();

        void add(final NerToken token) {
            final int tSeq = token.tokenId;
            final String category = token.entityMentionType;
            if (tSeq < lastSeq) {
//                System.out.println("NEW SENTENCE");
            } else {
                if (category != lastCategory) { // category change
                    if (lastCategory != CAT_OTHER) {
                        final Mention mention = finishPhrase(lastCategory);
//                        System.out.println("mention = " + mention);
                        mentions.add(mention);
                    }
                    if (category != CAT_OTHER) {
                        startPhrase(token);
                    }
                    lastCategory = category;
                } else if (category != CAT_OTHER) {
                    extendPhrase(token);
                }

            }
            lastSeq = tSeq;
        }

        Mention[] getMentions() {
            final Mention[] array = mentions.toArray(new Mention[mentions.size()]);
            mentions.clear();
            return array;
        }

        private void startPhrase(final NerToken nerToken) {
            sbToken.append(nerToken.token);
            sbLemma.append(nerToken.lemma);
        }

        private void extendPhrase(final NerToken nerToken) {
            sbToken.append(' ').append(nerToken.token);
            sbLemma.append(' ').append(nerToken.lemma);
        }

        private Mention finishPhrase(final String category) {
            final Mention mention = new Mention(category, sbToken.toString(), sbLemma.toString());
            sbToken.setLength(0);    // reset
            sbLemma.setLength(0);    // reset
            return mention;
        }
    }

    static final class ConstantWeigthsOf1 extends ArrayList<Double> {
        private final static Double constantValue = 1.0;
        final int size;

        ConstantWeigthsOf1(int size) {
            super(0);
            this.size = size;
        }

        @Override
        public int size() {
            return size;
        }

        @Override
        public Double get(int index) {
            return constantValue;
        }
    }

    static BasicDBObject createMongoMention(final Mention mention, final ArrayList<String> bagOfWords) {
        final BasicDBObject mentionObject = new BasicDBObject("name", mention.text);
        final BasicDBObject bow = new BasicDBObject(2);
        bow.put("words", bagOfWords);
        bow.put("weights", new ConstantWeigthsOf1(bagOfWords.size()));
        mentionObject.put("bow", bow);
        mentionObject.put("isMention", Boolean.TRUE);
        return mentionObject;
    }
}
