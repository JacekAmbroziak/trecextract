package com.diffeo.pipeline;

import com.diffeo.input.StringEscapePythonUtils;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;

import java.util.ArrayList;
import java.util.regex.Pattern;

/**
 * Copyright 2012 Diffeo, Inc. All Rights Reserved.
 * This software is the proprietary information of Diffeo, Inc.
 * Use is subject to license terms.
 *
 * @author Jacek R. Ambroziak
 */
final class ParseJsonStageStreaming extends Stage {
    private final Stage source;
    private final ArrayList<String> urlNames = new ArrayList<String>();
    private static final Pattern TAB_NEWLINE = Pattern.compile("[\n\t]");
    private ArrayList<Sentence> sentences = new ArrayList<Sentence>();
    private int sentenceCount;
    private int currentSentenceIndex;
    private boolean inDocument;
    private JsonFactory jsonFactory = new JsonFactory();
    private int lineCount;

    ParseJsonStageStreaming(Stage source) {
        this.source = source;
    }

    @Override
    Datum next() {
        // return from sentence buffer if not exhausted
        if (inDocument) {
            if (++currentSentenceIndex < sentenceCount) {
                return sentences.get(currentSentenceIndex);
            } else {
                inDocument = false;
                return EndDocument.INSTANCE;
            }
        }
        // otherwise try to refill sentence buffer
        final Datum datum = source.next();
        if (datum instanceof InputTextLine) {
            ++lineCount;
            urlNames.clear();
            sentences.clear();

            String docId = "";
            String streamId = "";
            String absUrl = "";
            String cleansed = "";
            try {
                {
                    final JsonParser jp = jsonFactory.createJsonParser(((InputTextLine) datum).line);
                    JsonToken jsonToken;
                    while ((jsonToken = jp.nextToken()) != null) {
                        if (jsonToken == JsonToken.FIELD_NAME) {
                            final String name = jp.getCurrentName();
                            if (name.equals("abs_url")) {
                                absUrl = jp.nextTextValue();
                            } else if (name.equals("cleansed")) {
                                cleansed = jp.nextTextValue();
                            } else if (name.equals("ner")) {
                                readNerSentences(jp.nextTextValue(), sentences);
                                sentenceCount = sentences.size();
                                currentSentenceIndex = -1;
                                inDocument = true;
                            } else if (name.equals("doc_id")) {
                                docId = jp.nextTextValue();
                            } else if (name.equals("stream_id")) {
                                streamId = jp.nextTextValue();
                            } else if (name.equals("urlname")) {
                                urlNames.add(jp.nextTextValue());
                            }
                        }
                    }
                    jp.close();
                }
                final StartDocument doc = new StartDocument(docId, urlNames);
                doc.absUrl = absUrl;
                doc.streamId = streamId;
                doc.cleansedEscaped = cleansed;
                return doc;
            } catch (Exception e) {
                return new StageException(e);
            }
        } else {
            if (datum == EndOfData.INSTANCE) {
                System.out.println("lineCount = " + lineCount);
            }
            return datum;
        }
    }

    private static void readNerSentences(final String nerText, final ArrayList<Sentence> sentences) throws Exception {
        final byte[] bytes = StringEscapePythonUtils.unescapePython(nerText);
        // making NerTokens from data in JSON can be speed optimized; simple 'split' for now
        final String unescaped = new String(bytes);
        final String[] parts = TAB_NEWLINE.split(unescaped);
        final int partCount = parts.length;
        final ArrayList<NerPhrase> tokens = new ArrayList<NerPhrase>();
        int lineNumber = 0;
        for (int offset = 0; offset < partCount; offset += 7) {
            if (parts[offset].isEmpty()) {  // at end of sentence 2 newlines...
                sentences.add(new Sentence(tokens.toArray(new NerPhrase[tokens.size()])));
                tokens.clear();
                ++offset;   // skip empty string at end of sentence
                ++lineNumber;
            }
            final NerToken nerToken = new NerToken(Integer.parseInt(parts[offset]), // tokenId
                    parts[offset + 1],  // token
                    parts[offset + 2],  // lemma
                    parts[offset + 3],  // pos
                    parts[offset + 4],  // entity type
                    Integer.parseInt(parts[offset + 5]),    // start
                    Integer.parseInt(parts[offset + 6]),
                    lineNumber++);   // end
            tokens.add(nerToken);
        }
    }
}
