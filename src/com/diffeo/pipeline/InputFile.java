package com.diffeo.pipeline;

import java.io.File;

/**
 * Copyright 2012 Diffeo, Inc. All Rights Reserved.
 * This software is the proprietary information of Diffeo, Inc.
 * Use is subject to license terms.
 *
 * @author Jacek R. Ambroziak
 */
final class InputFile extends Datum {
    private final File file;

    public InputFile(File file) {
        this.file = file;
    }

    public File getFile() {
        return file;
    }
}