package com.diffeo.pipeline;

/**
 * Copyright 2012 Diffeo, Inc. All Rights Reserved.
 * This software is the proprietary information of Diffeo, Inc.
 * Use is subject to license terms.
 *
 * @author Jacek R. Ambroziak
 */
public class FastNLSplitTest {
    public static void main(String[] args) {
        cut("abc\ndef\nghi");
        System.out.println("---");
        cut("abc\ndef");
        System.out.println("---");
        cut("abc");
    }

    private static void cut(final String s) {
        final int lastNL = s.lastIndexOf('\n');
        if (lastNL > 0) {
            for (int start = 0; ; ) {
                final int nlIdx;
                final String ss = s.substring(start, nlIdx = s.indexOf('\n', start));   // guaranteed to be > 0
                System.out.println("ss = " + ss);
                if (nlIdx != lastNL) {  // just one test
                    start = nlIdx + 1;
                } else {
                    break;
                }
            }
        }
        final String ss = s.substring(lastNL + 1, s.length());
        System.out.println("ss = " + ss);
    }
}
