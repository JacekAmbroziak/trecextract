package com.diffeo.pipeline;

import java.io.IOException;
import java.util.ArrayList;
import java.util.TreeSet;

final class RedirectCanopize extends Stage {
    private final Stage source;
    private final RedirectEntityLexicon entityLexicon;
    private ArrayList<NerToken> tokenList = new ArrayList<NerToken>(); // auxiliary, reusable list

    RedirectCanopize(Stage source, RedirectEntityLexicon entityLexicon) throws IOException {
        this.source = source;
        this.entityLexicon = entityLexicon;
    }

    @Override
    Datum next() {
        final Datum datum = source.next();
        if (datum instanceof Mention) {
            //System.err.println(((Mention)datum).name);
            addCanopies((Mention) datum);
        }
        return datum;
    }

    private void addCanopies(final Mention mention) {
        tokenList.clear();
        for (final NerToken token : mention.loadTokens(tokenList)) {

            //System.err.println(token);

            final TreeSet<Entity> entities = entityLexicon.lowercaseTokenMatches(token.token.toLowerCase());


            if (entities != null) {

//                for(Entity e : entities)
//                {
//                    System.out.println(e.label + " and the token: " + token);
//
//                }


                for (Entity entDesc : entities) {
                    mention.canopies.add(entDesc.label);
                }
            }
        }
    }
}
