package com.diffeo.pipeline;

import java.util.*;

final class Mention extends Datum implements Cloneable {
    private final static List<Entity> EMPTY = Collections.emptyList();

    final String name;
    UUID uuid;
    String[] bow;
    NerPhrase phrase;
    Sentence prevSentence;
    Sentence sentence;
    Sentence nextSentence;
    TreeSet<String> canopies = new TreeSet<String>();
    String label;
    List<Entity> matchingEntities = EMPTY;

    Mention(String name) {
        this.name = name;
        uuid = UUID.randomUUID();
    }

    void setBow(final ArrayList<String> bow) {
        this.bow = bow.toArray(new String[bow.size()]);
    }

    String window() {   // up to 3 sentences
        final StringBuilder sb = new StringBuilder(512);
//        if (prevSentence != null) {
//            prevSentence.appendAsText(sb);
//        }
//        sentence.appendAsText(sb);
        sentence.appendAsTextWithSpan(sb, phrase);
//        if (nextSentence != null) {
//            nextSentence.appendAsText(sb);
//        }
        return sb.toString();
    }

    ArrayList<NerToken> loadTokens(ArrayList<NerToken> tokens) {
        phrase.append(tokens);
        return tokens;
    }

    ArrayList<String> getLowerCaseTokens() {
        final ArrayList<NerToken> nerTokens = new ArrayList<NerToken>(8);
        loadTokens(nerTokens);
        final ArrayList<String> result = new ArrayList<String>(nerTokens.size());
        for (final NerToken nerToken : nerTokens) {
            result.add(nerToken.token.toLowerCase());
        }
        return result;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        final Mention clone = (Mention) super.clone();
        clone.uuid = UUID.randomUUID();
        clone.canopies = new TreeSet<String>();
        return clone;
    }
}
