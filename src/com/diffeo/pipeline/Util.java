package com.diffeo.pipeline;

/**
 * Copyright 2012 Diffeo, Inc. All Rights Reserved.
 * This software is the proprietary information of Diffeo, Inc.
 * Use is subject to license terms.
 *
 * @author Jacek R. Ambroziak
 */
public final class Util {
    static String join(final String[] strings) {
        return join(strings, ' ');
    }

    static String join(final String[] strings, final char link) {
        if (strings != null) {
            final int last = strings.length - 1;
            switch (last) {
                case -1:
                    return "";

                case 0:
                    return strings[0];

                default:
                    final StringBuilder sb = new StringBuilder(64);
                    for (int i = 0; i < last; i++) {
                        sb.append(strings[i]).append(link);
                    }
                    return sb.append(strings[last]).toString();
            }
        } else {
            return null;
        }
    }
}
