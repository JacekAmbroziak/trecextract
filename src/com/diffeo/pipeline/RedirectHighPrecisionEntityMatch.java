package com.diffeo.pipeline;

import java.io.IOException;
import java.util.ArrayList;
import java.util.TreeSet;

final class RedirectHighPrecisionEntityMatch extends Stage {
    private final Stage source;
    private final RedirectEntityLexicon entityLexicon;

    RedirectHighPrecisionEntityMatch(Stage source, RedirectEntityLexicon entityLexicon) throws IOException {
        this.source = source;
        this.entityLexicon = entityLexicon;
    }

    @Override
    Datum next() {
        final Datum datum = source.next();
        if (datum instanceof Mention) {
            //Mention m = (Mention) datum;
            //System.err.println(m.toString());
            setMatches((Mention) datum);
        }
        return datum;
    }

    private void setMatches(final Mention mention) {


        ArrayList<Entity> matches = new ArrayList<Entity>();


        TreeSet<Entity> matchSet = entityLexicon.phraseMatches(mention.phrase.text().toLowerCase());

        if (mention.name.equals("Aharon Barak")) {
            System.err.println(mention.phrase.text().toLowerCase());
        }

        if (matchSet != null) {

            for (Entity eMatch : entityLexicon.phraseMatches(mention.phrase.text().toLowerCase())) {
                matches.add(eMatch);
            }


            mention.matchingEntities = matches;

        }


    }
}
