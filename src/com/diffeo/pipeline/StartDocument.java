package com.diffeo.pipeline;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Copyright 2012 Diffeo, Inc. All Rights Reserved.
 * This software is the proprietary information of Diffeo, Inc.
 * Use is subject to license terms.
 *
 * @author Jacek R. Ambroziak
 */
final class StartDocument extends Datum {
    private static final String[] EMPTY = new String[0];
    final String docId;
    final String[] urlNames;
    String absUrl;
    String cleansedEscaped;
    String streamId;

    StartDocument(String docId, ArrayList<String> urlNames) {
        this.docId = docId;
        final int n = urlNames.size();
        this.urlNames = n == 0 ? EMPTY : urlNames.toArray(new String[n]);
    }

    @Override
    public String toString() {
        return "StartDocument " + docId + ' ' + Arrays.toString(urlNames) + '\t' + absUrl;
    }
}
