package com.diffeo.pipeline;

final class EndDocument extends Datum {
    static final EndDocument INSTANCE = new EndDocument();

    private EndDocument() {
        // singleton
    }

    @Override
    public String toString() {
        return "EndDocument";
    }
}
