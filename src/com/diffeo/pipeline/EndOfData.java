package com.diffeo.pipeline;

/**
 * Copyright 2012 Diffeo, Inc. All Rights Reserved.
 * This software is the proprietary information of Diffeo, Inc.
 * Use is subject to license terms.
 *
 * @author Jacek R. Ambroziak
 */
final class EndOfData extends StreamEvent {
    static final EndOfData INSTANCE = new EndOfData();

    private EndOfData() {
        // singleton
    }
}
