package com.diffeo.pipeline;

/**
 * Copyright 2012 Diffeo, Inc. All Rights Reserved.
 * This software is the proprietary information of Diffeo, Inc.
 * Use is subject to license terms.
 *
 * @author Jacek R. Ambroziak
 */
public final class MongoPipeline {
    public static void main(String[] args) {
        try {
            final Config config = new Config(args);
            final EntityLexicon entityLexicon = new EntityLexicon("config/entity-urlnames-old.json");
//            entityLexicon.readRedirects("config/redirects.json");
            final Stage source = config.getSource();
            final UnzipTextLines unzipper = new UnzipTextLines(source);
            final ParseJsonStage parseJsonStage = new ParseJsonStage(unzipper);
            // group same "entity type" NER labeled tokens into phrases
            final MakeTokenGroups groupTokens = new MakeTokenGroups(parseJsonStage);
            // create Mention data from appropriate NerPhrases and supply them with context info
            // such as surrounding sentences and "bags of words"
            final BuildMentions buildMentions = new BuildMentions(groupTokens);
            // supply Mentions with canopy labels
            final Canopize canopize = new Canopize(buildMentions, entityLexicon);
            // drop Mentions with no canopy labels
            final FilterMentions filterMentions = new FilterMentions(canopize);
            // split Mentions with multiple canopy labels into multiple Mentions
            final SingleCanopyMentions singleCanopyMentions = new SingleCanopyMentions(filterMentions);
            final LabelMentions labelMentions = new LabelMentions(singleCanopyMentions, entityLexicon);
            final StoreMentionsInMongo sink = new StoreMentionsInMongo(labelMentions, "coref5", "mentions");
            {
                final long start = System.currentTimeMillis();
                int counter = 0;
                Datum datum;
                while ((datum = sink.next()) != EndOfData.INSTANCE) {
                    System.out.println(datum);
                    ++counter;
                }
                final long end = System.currentTimeMillis();
                System.err.println("DONE " + (end - start) + " msec");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
