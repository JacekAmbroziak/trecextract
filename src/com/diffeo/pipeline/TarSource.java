package com.diffeo.pipeline;

import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.GZIPInputStream;

/**
 * Copyright 2012 Diffeo, Inc. All Rights Reserved.
 * This software is the proprietary information of Diffeo, Inc.
 * Use is subject to license terms.
 *
 * @author Jacek R. Ambroziak
 */
final class TarSource extends Stage {
    private final File tarFile;
    private TarArchiveInputStream tarArchiveInputStream;
    private boolean isClosed;

    TarSource(File tarFile) {
        this.tarFile = tarFile;
    }

    TarSource(String path) {
        this(new File(path));
    }

    @Override
    Datum next() {
        if (isClosed) {
            return EndOfData.INSTANCE;
        } else if (tarArchiveInputStream != null) {
            try {
                final FileData fileData = readTarEntry(tarArchiveInputStream);
                return fileData != null ? fileData : EndOfData.INSTANCE;
            } catch (IOException e) {
                return new StageException(e);
            }
        } else {
            try {
                final String fileName = tarFile.getName();
                final InputStream is;
                final FileInputStream fileInputStream = new FileInputStream(tarFile);
                if (fileName.endsWith(".tar.gz")) {
                    is = new GZIPInputStream(fileInputStream);
                } else if (fileName.endsWith(".tar")) {
                    is = fileInputStream;
                } else {
                    throw new IOException("Not an archive: " + fileName);
                }
                tarArchiveInputStream = new TarArchiveInputStream(is);
                return next();
            } catch (IOException e) {
                return new StageException(e);
            }
        }
    }

    private FileData readTarEntry(final TarArchiveInputStream tis) throws IOException {
        final TarArchiveEntry entry = tis.getNextTarEntry();
        if (entry != null) {
            final String name = entry.getName();
            final int size = (int) entry.getSize();
            final byte[] buf = new byte[size];
            int count = 0;
            do {
                count += tis.read(buf, count, size - count);
            } while (count < size);
            return new FileData(name, buf);
        } else {
            tis.close();
            isClosed = true;
            return null;
        }
    }

    // test only
    public static void main(String[] args) {
        try {
            test("/mnt/data1/diffeo/data/oneday.2012-02-01.tar");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void test(String tarName) throws IOException {
        TarArchiveInputStream tis = new TarArchiveInputStream(new FileInputStream(tarName));
        try {
            TarArchiveEntry tarArchiveEntry;
            while ((tarArchiveEntry = tis.getNextTarEntry()) != null) {
                String name = tarArchiveEntry.getName();
                final int size = (int) tarArchiveEntry.getSize();
                System.out.println("docId = " + name + "\t" + size);
                byte[] buf = new byte[size];
                int count = 0;
                do {
                    count += tis.read(buf, count, size - count);
                } while (count < size);
                System.out.println("OK");
            }
        } finally {
            tis.close();
        }
    }
}
