package com.diffeo.pipeline;

import java.io.*;
import java.util.ArrayList;
import java.util.zip.GZIPInputStream;

/**
 * Copyright 2012 Diffeo, Inc. All Rights Reserved.
 * This software is the proprietary information of Diffeo, Inc.
 * Use is subject to license terms.
 *
 * @author Jacek R. Ambroziak
 */
final class UnzipTextLines extends Stage {
    private final Stage source;
    private ArrayList<String> lines;
    private int lineCount;
    private int lineIndex;
    private String currentFileName;

    UnzipTextLines(Stage source) {
        this.source = source;
    }

    @Override
    Datum next() {
        if (lines != null) {
            final int index = ++lineIndex;
            if (index < lineCount) {
                return new InputTextLine(lines.get(index));
            } else {
                lines = null;   // GC and state == 'buffer empty'
                return new FileEvent(currentFileName, FileEvent.Status.END);
            }
        }
        // if not returned above
        final Datum next = source.next();
        if (next instanceof InputFile) {
            try {   // eager consumption of the file
                final File file = ((InputFile) next).getFile();
                readData(new FileInputStream(file));
                return new FileEvent(currentFileName = file.getName(), FileEvent.Status.START);
            } catch (IOException e) {
                return new StageException(e);
            }
        } else if (next instanceof FileData) {
            final FileData fileData = (FileData) next;
            currentFileName = fileData.getFileName();
            try {
                readData(new ByteArrayInputStream(fileData.getData()));
            } catch (IOException e) {
                return new StageException(e);
            }
            return new FileEvent(fileData.getFileName(), FileEvent.Status.START);
        } else {
            return next;    // pass-thru 'control' events (start, end, end-of-data)
        }
    }

    private void readData(final InputStream inputStream) throws IOException {
        lines = readLines(new GZIPInputStream(inputStream));
        lineCount = lines.size();
        lineIndex = -1; // before 1st line
    }

    private static ArrayList<String> readLines(final InputStream is) throws IOException {
        final ArrayList<String> lines = new ArrayList<String>();
        final BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        String line;
        while ((line = reader.readLine()) != null) {
            lines.add(line);
        }
        reader.close();
        return lines;
    }
}
