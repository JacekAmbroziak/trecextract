package com.diffeo.pipeline;

/**
 * Copyright 2012 Diffeo, Inc. All Rights Reserved.
 * This software is the proprietary information of Diffeo, Inc.
 * Use is subject to license terms.
 *
 * @author Jacek R. Ambroziak
 */
final class FileEvent extends StreamEvent {
    private final String fileName;
    private final Status status;

    public FileEvent(String fileName, Status status) {
        this.fileName = fileName;
        this.status = status;
    }

    enum Status {
        START, END
    }

    @Override
    public String toString() {
        return "FileEvent\t" + fileName + ' ' + (status == Status.START ? "START" : "END");
    }
}
