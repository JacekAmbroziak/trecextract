package com.diffeo.pipeline;

/**
 * Copyright 2012 Diffeo, Inc. All Rights Reserved.
 * This software is the proprietary information of Diffeo, Inc.
 * Use is subject to license terms.
 *
 * @author Jacek R. Ambroziak
 */
final class InputTextLine extends Datum {
    final String line;

    InputTextLine(String line) {
        this.line = line;
    }

    @Override
    public String toString() {
        return line;
    }
}
