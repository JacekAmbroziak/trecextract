package com.diffeo.pipeline;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.regex.Pattern;

/**
 * Copyright 2012 Diffeo, Inc. All Rights Reserved.
 * This software is the proprietary information of Diffeo, Inc.
 * Use is subject to license terms.
 *
 * @author Dan Roberts and Jacek R. Ambroziak
 */
final class RedirectEntityLexicon {
    static final Comparator<Entity> ENTITY_DESCRIPTION_COMPARATOR = new Comparator<Entity>() {
        @Override
        public int compare(Entity o1, Entity o2) {
            return o1.label.compareTo(o2.label);
        }
    };
    private HashMap<String, Entity> entities = new HashMap<String, Entity>(128);
    // map from phrases like "Paul Graham" to sets of potentially matching entity labels like "Paul_Graham_(computer_programmer)"
    private HashMap<String, TreeSet<Entity>> phrase2Entities = new HashMap<String, TreeSet<Entity>>(128);
    // map from tokens (lowercased) to entity labels
    private HashMap<String, TreeSet<Entity>> tokens2Entities = new HashMap<String, TreeSet<Entity>>(128);

    RedirectEntityLexicon(final String path, final String redirectPath) throws Exception {

        readEntities(path);

        readRedirects(redirectPath);
    }

    void readRedirects(final String path) throws Exception {
        final JsonNode rootNode = new ObjectMapper().readValue(new File(path), JsonNode.class);
        if (rootNode.isObject()) {
            final ObjectNode dictionary = (ObjectNode) rootNode;
            final Iterator<Map.Entry<String, JsonNode>> fields = dictionary.fields();
            while (fields.hasNext()) {
                final Map.Entry<String, JsonNode> entry = fields.next();
                final String entityLabel = entry.getKey();
                final ArrayNode redirects = (ArrayNode) entry.getValue();
                final int redirectCount = redirects.size();
                if (redirectCount > 0) {
                    //System.err.println("entityLabel = " + entityLabel);
                    //System.err.println("redirects = " + redirects);
                    final Entity ed = entities.get(entityLabel);
                    if (ed != null) {
                        final String[] strings = new String[redirectCount];
                        for (int i = redirectCount; --i >= 0; ) {
                            strings[i] = redirects.get(i).asText();
                        }
                        ed.redirects = strings;

                        indexRedirectDescriptionByPhrase(ed, phrase2Entities);
                        indexRedirectDescriptionByLowercaseTokens(ed, tokens2Entities);

                    } else {
                        throw new Exception("unknown entity " + entityLabel);
                    }
                }
            }
        }
    }

    private void readEntities(final String path) throws IOException {
        final JsonNode rootNode = new ObjectMapper().readValue(new File(path), JsonNode.class);
        if (rootNode.isArray()) {
            final Pattern underscore = Pattern.compile("_");
            final ArrayNode array = (ArrayNode) rootNode;
            final int count = array.size();
            for (int i = 0; i < count; i++) {
                final String entityLabel = array.get(i).asText().trim();
                final int lpar = entityLabel.indexOf('(');  // e.g. Paul_Graham_(computer_programmer)
                // split by underscore but only the part of label before '('
                final String[] tokens = underscore.split(lpar < 0 ? entityLabel : entityLabel.substring(0, lpar));
                final Entity entDesc = new Entity(entityLabel);
                entDesc.tokens = Arrays.asList(tokens);
                entDesc.phrase = Util.join(tokens);
                indexEntityDescriptionByPhrase(entDesc, phrase2Entities);
                indexEntityDescriptionByLowercaseTokens(entDesc, tokens2Entities);
                entities.put(entityLabel, entDesc);
            }
        }
    }

    TreeSet<Entity> phraseMatches(final String phrase) {  // "Paul Graham"
        return phrase2Entities.get(phrase);
    }

    TreeSet<Entity> lowercaseTokenMatches(final String lowercaseToken) {  // "paul"
        return tokens2Entities.get(lowercaseToken);
    }

    List<Entity> allTokensMatch(final String[] tokens) {
        TreeSet<Entity> set = tokens2Entities.get(tokens[0]);
        if (set == null) {  // no match on 1st token, so already dead...
            return Collections.emptyList();
        } else {
            final ArrayList<Entity> result = new ArrayList<Entity>(set.size());
            result.addAll(set);
            // we want to retain EntityDescriptions that match every token
            for (int i = tokens.length - 1; i > 0; --i) {
                final String token = tokens[i];
                int index = result.size() - 1;
                do {
                    if (result.get(index).tokens.contains(token)) {
                        // good...
                    } else {
                        result.remove(index);   // didn't match the current token
                        if (result.isEmpty()) { // if the result has just become empty, no more work
                            return result;
                        }
                    }
                } while (--index >= 0);
            }
            return result;
        }
    }

    List<Entity> allEntityTokensMatch(final ArrayList<String> tokens) {
        final HashSet<Entity> matchingEntities = new HashSet<Entity>();
        for (final String token : tokens) {
            final TreeSet<Entity> descriptions = tokens2Entities.get(token);
            if (descriptions != null) {
                matchingEntities.addAll(descriptions);
            }
        }
        if (matchingEntities.isEmpty()) {
            return Collections.emptyList();
        } else {
            final ArrayList<Entity> candidates = new ArrayList<Entity>(matchingEntities);
            for (int i = candidates.size(); --i >= 0; ) {
                for (final String entityToken : candidates.get(i).tokens) {
                    if (tokens.contains(entityToken.toLowerCase())) {
                        // good
                    } else {
                        candidates.remove(i);
                        break;
                    }
                }
            }
            return candidates;  // pruned candidates
        }
    }

    private static void indexEntityDescriptionByPhrase(final Entity entity, final HashMap<String, TreeSet<Entity>> phrase2Entities) {
        final String phrase = entity.phrase; // e.g. "Paul Graham"
        TreeSet<Entity> entDescSet = phrase2Entities.get(phrase);
        if (entDescSet == null) {
            phrase2Entities.put(phrase, entDescSet = new TreeSet<Entity>(ENTITY_DESCRIPTION_COMPARATOR));
        }
        entDescSet.add(entity);
    }

    private static void indexEntityDescriptionByLowercaseTokens(final Entity entDesc,
                                                                final HashMap<String, TreeSet<Entity>> tokens2Entities) {
        for (final String token : entDesc.tokens) {
            final String lowercased = token.toLowerCase();
            TreeSet<Entity> entDescSet = tokens2Entities.get(lowercased);
            if (entDescSet == null) {
                tokens2Entities.put(lowercased, entDescSet = new TreeSet<Entity>(ENTITY_DESCRIPTION_COMPARATOR));
            }
            entDescSet.add(entDesc);
        }
    }

    private static void indexRedirectDescriptionByPhrase(final Entity entity, final HashMap<String, TreeSet<Entity>> phrase2Entities) {
        for (final String redirectPhrase : entity.redirects) {
            String redirectPhraseLower = redirectPhrase.toLowerCase();
            TreeSet<Entity> entDescSet = phrase2Entities.get(redirectPhraseLower);
            if (entDescSet == null) {
                phrase2Entities.put(redirectPhraseLower, entDescSet = new TreeSet<Entity>(ENTITY_DESCRIPTION_COMPARATOR));
            }
            entDescSet.add(entity);
        }
    }

    private static void indexRedirectDescriptionByLowercaseTokens(final Entity entDesc,
                                                                  final HashMap<String, TreeSet<Entity>> tokens2Entities) {

        for (final String redirectPhrase : entDesc.redirects) {
            for (final String redirectToken : redirectPhrase.split(" ")) {
                final String lowercased = redirectToken.toLowerCase();
                TreeSet<Entity> entDescSet = tokens2Entities.get(lowercased);
                if (entDescSet == null) {
                    tokens2Entities.put(lowercased, entDescSet = new TreeSet<Entity>(ENTITY_DESCRIPTION_COMPARATOR));
                }
                entDescSet.add(entDesc);
            }
        }
    }
}
