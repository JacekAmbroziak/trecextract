package com.diffeo.pipeline;

import com.diffeo.input.StringEscapePythonUtils;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;

import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Pattern;

/**
 * Copyright 2012 Diffeo, Inc. All Rights Reserved.
 * This software is the proprietary information of Diffeo, Inc.
 * Use is subject to license terms.
 *
 * @author Jacek R. Ambroziak
 */
final class ParseJsonStage extends Stage {
    private final Stage source;
    private final ObjectMapper mapper = new ObjectMapper(); // Jackson parse-to-tree util
    private final ArrayList<String> urlNames = new ArrayList<String>();
    private static final Pattern TAB_NEWLINE = Pattern.compile("[\n\t]");
    private ArrayList<Sentence> sentences = new ArrayList<Sentence>();
    private int sentenceCount;
    private int currentSentenceIndex;
    private boolean inDocument;
    private int lineCount;

    ParseJsonStage(Stage source) {
        this.source = source;
    }

    @Override
    Datum next() {
        // return from sentence buffer if not exhausted
        if (inDocument) {
            if (++currentSentenceIndex < sentenceCount) {
                return sentences.get(currentSentenceIndex);
            } else {
                inDocument = false;
                return EndDocument.INSTANCE;
            }
        }
        // otherwise try to refill sentence buffer
        final Datum datum = source.next();
        if (datum instanceof InputTextLine) {
            ++lineCount;
            final String line = ((InputTextLine) datum).line;
            try {
                final JsonNode rootNode = mapper.readValue(line, JsonNode.class);
                readUrlNames(rootNode, urlNames);
                try {
                    sentences.clear();
                    readNerSentences(rootNode, sentences);
                    sentenceCount = sentences.size();
                    currentSentenceIndex = -1;
                    inDocument = true;
                    final JsonNode body = rootNode.get("body");
                    final StartDocument doc = new StartDocument(rootNode.get("doc_id").asText(), urlNames);
                    doc.absUrl = rootNode.get("abs_url").asText();
                    doc.streamId = rootNode.has("stream_id") ? rootNode.get("stream_id").asText() : "";
                    doc.cleansedEscaped = body != null && body.has("cleansed") ? body.get("cleansed").asText() : "";
                    return doc;
                } catch (Exception e) {
                    return new StageException(e);
                }
            } catch (IOException e) {
                return new StageException(e);
            }
        } else {
            if (datum == EndOfData.INSTANCE) {
                System.err.println("ParseJson: lineCount = " + lineCount);
            }
            return datum;
        }
    }

    private static void readUrlNames(final JsonNode rootNode, final ArrayList<String> urlNames) {
        urlNames.clear();
        if (rootNode.has("source_metadata")) {
            final JsonNode sourceMetadata = rootNode.get("source_metadata");
            if (sourceMetadata.has("entities")) {
                final JsonNode entities = sourceMetadata.get("entities");
                if (entities.isArray()) {
                    final ArrayNode entitiesAsArrayNode = (ArrayNode) entities;
                    final int size = entitiesAsArrayNode.size();
                    for (int i = 0; i < size; ++i) {
                        final JsonNode urlName = entitiesAsArrayNode.get(i).get("urlname");
                        if (urlName != null) {
                            urlNames.add(urlName.textValue());
                        }
                    }
                }
            }
        }
    }

    private static void readNerSentences(final JsonNode rootNode, final ArrayList<Sentence> sentences) throws Exception {
        if (rootNode.has("body")) {
            final JsonNode body = rootNode.get("body");
            if (body.has("ner")) {
                String escapedString = body.get("ner").textValue();
                final String unescaped = StringEscapePythonUtils.unescapePythonToString(escapedString);
                // making NerTokens from data in JSON can be speed optimized; simple 'split' for now
                final String[] parts = TAB_NEWLINE.split(unescaped);
                final int partCount = parts.length;
                final ArrayList<NerPhrase> tokens = new ArrayList<NerPhrase>();
                int lineNumber = 0;
                for (int offset = 0; offset < partCount; offset += 7) {
                    if (parts[offset].isEmpty()) {  // at end of sentence 2 newlines...
                        sentences.add(new Sentence(tokens.toArray(new NerPhrase[tokens.size()])));
                        tokens.clear();
                        ++offset;   // skip empty string at end of sentence
                        ++lineNumber;
                    }
                    final NerToken nerToken = new NerToken(Integer.parseInt(parts[offset]), // tokenId
                            parts[offset + 1],  // token
                            parts[offset + 2],  // lemma
                            parts[offset + 3],  // pos
                            parts[offset + 4],  // entity type
                            Integer.parseInt(parts[offset + 5]),    // start
                            Integer.parseInt(parts[offset + 6]),
                            lineNumber++);   // end
                    tokens.add(nerToken);
                }
            }
        }
    }
}
