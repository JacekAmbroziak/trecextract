package com.diffeo.pipeline;

import java.util.ArrayList;

/**
 * This stage will replace Mentions containing multiple canopy name strings ("canopies")
 * with multiple cloned Mentions containing single canopies
 */

final class SingleCanopyMentions extends Stage {
    private final Stage source;
    private final ArrayList<Mention> mentions = new ArrayList<Mention>();

    SingleCanopyMentions(Stage source) {
        this.source = source;
    }

    @Override
    Datum next() {
        try {
            final int mentionCount = mentions.size();
            if (mentionCount == 0) {    // queue of Mentions is empty
                final Datum datum = source.next();
                if (datum instanceof Mention) {
                    final Mention mention = (Mention) datum;
                    final int size = mention.canopies.size();
                    if (size > 1) {
                        final ArrayList<String> canopyList = new ArrayList<String>(size);
                        canopyList.addAll(mention.canopies);
                        mention.canopies.clear();   // don't clone the set of canopies
                        for (int i = 1; i < size; ++i) {    // all but first; we need size-1 clones
                            final Mention clone = (Mention) mention.clone();    // clone will create new UUID and new canopy set
                            clone.canopies.add(canopyList.get(i));
                            mentions.add(clone);
                        }
                        mention.canopies.add(canopyList.get(0));
                        return mention; // return the original, modified Mention
                    }
                }
                return datum;   // passthru non-Mention data and Mentions with 0 or 1 canopy
            } else {
                return mentions.remove(mentionCount - 1);   // return a queued up cloned Mention
            }
        } catch (CloneNotSupportedException e) {
            return new StageException(e);
        }
    }
}
