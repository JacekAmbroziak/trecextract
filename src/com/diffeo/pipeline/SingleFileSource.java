package com.diffeo.pipeline;

import java.io.File;

/**
 * Copyright 2012 Diffeo, Inc. All Rights Reserved.
 * This software is the proprietary information of Diffeo, Inc.
 * Use is subject to license terms.
 *
 * @author Jacek R. Ambroziak
 */
final class SingleFileSource extends Stage {
    private final File file;
    private boolean finished;

    SingleFileSource(File file) {
        this.file = file;
    }

    SingleFileSource(String path) {
        this(new File(path));
    }

    @Override
    Datum next() {
        if (finished) {
            return EndOfData.INSTANCE;
        } else {
            finished = true;
            return new InputFile(file);
        }
    }
}
