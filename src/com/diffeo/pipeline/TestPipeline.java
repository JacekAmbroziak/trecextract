package com.diffeo.pipeline;

/**
 * Copyright 2012 Diffeo, Inc. All Rights Reserved.
 * This software is the proprietary information of Diffeo, Inc.
 * Use is subject to license terms.
 *
 * @author Jacek R. Ambroziak
 */
public final class TestPipeline {
    public static void main(String[] args) {
        try {
            final Config config = new Config(args);
            final EntityLexicon entityLexicon = new EntityLexicon("config/entity-urlnames.json");
            for (int i = 0; i < 500; i++) {
                final Stage source = config.getSource();
                final UnzipTextLines unzipper = new UnzipTextLines(source);
                final ParseJsonStage parseJsonStage = new ParseJsonStage(unzipper);
//                final ParseJsonStageStreaming parseJsonStage = new ParseJsonStageStreaming(unzipper);
                /*
                final MakeTokenGroups groupTokens = new MakeTokenGroups(parseJsonStage);
                // create Mention data from appropriate NerPhrases and supply them with context info
                // such as surrounding sentences and "bags of words"
                final BuildMentions buildMentions = new BuildMentions(groupTokens);
                // supply Mentions with canopy labels
//            final HighPrecisionCanopize canopize = new HighPrecisionCanopize(buildMentions, entityLexicon);
                final Canopize canopize = new Canopize(buildMentions, entityLexicon);
                // drop Mentions with no canopy labels
                final FilterMentions filterMentions = new FilterMentions(canopize);
                final HighPrecisionEntityMatch match = new HighPrecisionEntityMatch(filterMentions, entityLexicon);
                // split Mentions with multiple canopy labels into multiple Mentions
                final SingleCanopyMentions sink = new SingleCanopyMentions(match);
                       */
                final long start = System.currentTimeMillis();
                int counter = 0;
                Datum datum;
                while ((datum = parseJsonStage.next()) != EndOfData.INSTANCE) {
                    ++counter;
                }
                final long end = System.currentTimeMillis();
                System.out.println("counter = " + counter);
                System.out.println("msec = " + (end - start));
            }
            System.out.println("DONE");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
