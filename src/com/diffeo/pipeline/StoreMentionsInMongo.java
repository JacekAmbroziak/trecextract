package com.diffeo.pipeline;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.Mongo;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;

final class StoreMentionsInMongo extends Stage {
    private final Stage source;
    private final String dbName;
    private final String collName;
    private StartDocument document;

    StoreMentionsInMongo(Stage source, String dbName, String collName) {
        this.source = source;
        this.dbName = dbName;
        this.collName = collName;
    }

    @Override
    Datum next() {
        try {
            final Mongo mongo = new Mongo();
            final DB db = mongo.getDB(dbName);
            // db.dropDatabase();
            final DBCollection collection = db.getCollection(collName);
            // auxiliary, reusable lists
            final ArrayList<NerToken> nerTokens = new ArrayList<NerToken>();
            final ArrayList<String> tokens = new ArrayList<String>();
            Datum datum;
            while ((datum = source.next()) != EndOfData.INSTANCE) {
                if (datum instanceof Mention) {
                    collection.insert(createMongoMention((Mention) datum, nerTokens, tokens));
                } else if (datum instanceof StartDocument) {
                    setDocument((StartDocument) datum);         // set context Doc for the Mentions
                } else if (datum instanceof EndDocument) {
                    document = null;
                }
            }
            mongo.close();
            return datum;
        } catch (UnknownHostException e) {
            e.printStackTrace();
            return new StageException(e);
        }
    }

    private BasicDBObject createMongoMention(final Mention mention, ArrayList<NerToken> nerTokens, ArrayList<String> tokens) {
        nerTokens.clear();
        mention.loadTokens(nerTokens);  // fix: loading this again below
        final BasicDBObject mDbObj = new BasicDBObject("_id", document.streamId + ":" + nerTokens.get(0).lineNumber);
        mDbObj.put("isMention", Boolean.TRUE);
        mDbObj.put("ln", mention.name.replace(' ', '_'));    // coref5 replacement for FLName
        {   // building a bag of names from Mention's tokens
            nerTokens.clear();
            tokens.clear();
            for (NerToken nerToken : mention.loadTokens(nerTokens)) {
                tokens.add(nerToken.token);
            }
            if (tokens.size() > 1) {
                tokens.add(mention.name.replace(' ', '_'));
            }
//            mDbObj.put("tok", tokens);
            final BasicDBObject bow = new BasicDBObject(2);
            bow.put("words", tokens);
            bow.put("weights", new ConstantWeigthsOf1(tokens.size()));
            mDbObj.put("bagOfNames", bow);
        }
        {
            final String[] mBow = mention.bow;
            final BasicDBObject bow = new BasicDBObject(2);
            bow.put("words", Arrays.asList(mBow));
            bow.put("weights", new ConstantWeigthsOf1(mBow.length));
            mDbObj.put("bagOfKeywords", bow);
        }
        mDbObj.put("canopies", mention.canopies);
        mDbObj.put("canopy", mention.canopies.first()); // assuming single canopy
        if (mention.label != null) {
            mDbObj.put("lh", mention.label);
            System.out.println("mention = " + mention.label);
        } else {
            mDbObj.put("lh", "");
        }
        mDbObj.put("la", "");
        mDbObj.put("acnt", 0);
        mDbObj.put("priority", Math.exp(Math.random())); // random priority [0, e]
        return mDbObj;
    }

    private void setDocument(final StartDocument document) {
        this.document = document;
    }

    private static final class ConstantWeigthsOf1 extends ArrayList<Double> {
        private final static Double constantValue = 1.0;
        final int size;

        ConstantWeigthsOf1(int size) {
            super(0);
            this.size = size;
        }

        @Override
        public int size() {
            return size;
        }

        @Override
        public Double get(int index) {
            return constantValue;
        }
    }
}
