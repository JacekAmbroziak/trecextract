package com.diffeo.pipeline;

import java.io.IOException;

final class HighPrecisionEntityMatch extends Stage {
    private final Stage source;
    private final EntityLexicon entityLexicon;

    HighPrecisionEntityMatch(Stage source, EntityLexicon entityLexicon) throws IOException {
        this.source = source;
        this.entityLexicon = entityLexicon;
    }

    @Override
    Datum next() {
        final Datum datum = source.next();
        if (datum instanceof Mention) {
            setMatches((Mention) datum);
        }
        return datum;
    }

    private void setMatches(final Mention mention) {
        mention.matchingEntities = entityLexicon.allEntityTokensMatch(mention.getLowerCaseTokens());
    }
}
