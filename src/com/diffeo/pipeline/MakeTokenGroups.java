package com.diffeo.pipeline;

import java.util.ArrayList;

/**
 * Copyright 2012 Diffeo, Inc. All Rights Reserved.
 * This software is the proprietary information of Diffeo, Inc.
 * Use is subject to license terms.
 *
 * @author Jacek R. Ambroziak
 */
final class MakeTokenGroups extends Stage {
    static final String CAT_OTHER = "O".intern();
    private final Stage source;

    MakeTokenGroups(Stage source) {
        this.source = source;
    }

    @Override
    Datum next() {
        final Datum datum = source.next();
        if (datum instanceof Sentence) {
            return groupTokens((Sentence) datum);
        } else {
            return datum;   // passthru
        }
    }

    private static Sentence groupTokens(final Sentence input) {
        final NerPhrase[] tokens = input.phrases;   // should be all simple tokens at this stage
        final int tokenCount = tokens.length;
        final ArrayList<NerPhrase> phrases = new ArrayList<NerPhrase>(tokenCount);
        String lastCategory = CAT_OTHER;
        int indexOfFirst = -1;  // dummy beginning of group also meaning last category == OTHER
        for (int i = 0; i < tokenCount; ++i) {
            final NerToken token = (NerToken) tokens[i];
            final String entityType = token.entityMentionType;
            if (entityType.equals(lastCategory)) {  // continuation?
                if (indexOfFirst < 0) {     // don't care about grouping OTHER tokens
                    phrases.add(token);     // ...just add as is
                }
            } else {    // category change
                if (entityType.equals(CAT_OTHER)) { // change from 'good' to OTHER
                    phrases.add(previousPhrase(indexOfFirst, i, tokens));
                    phrases.add(token); // the new OTHER
                    indexOfFirst = -1;
                } else {    // change from good_1 (or initial) to good_2
                    if (indexOfFirst >= 0) {    // change from another good
                        phrases.add(previousPhrase(indexOfFirst, i, tokens));
                    }   // else change from initial == nothing to do
                    indexOfFirst = i;   // mark beginning
                }
                lastCategory = entityType;
            }
        }
        return new Sentence(phrases.toArray(new NerPhrase[phrases.size()]));
    }

    private static NerPhrase previousPhrase(int indexOfFirst, int current, NerPhrase[] tokens) {
        final int groupSize = current - indexOfFirst;
        if (groupSize == 1) {
            return tokens[indexOfFirst];
        } else {
            final NerPhrase[] group = new NerPhrase[groupSize];
            System.arraycopy(tokens, indexOfFirst, group, 0, groupSize);
            return new NerTokenGroup(group[0].getEntityMentionType(), group);
        }
    }
}
