package com.diffeo.pipeline;

import com.diffeo.input.StringEscapePythonUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.regex.Pattern;

/**
 * Copyright 2012 Diffeo, Inc. All Rights Reserved.
 * This software is the proprietary information of Diffeo, Inc.
 * Use is subject to license terms.
 *
 * @author Jacek R. Ambroziak
 */
final class ParseJsonStageTest extends Stage {
    private final Stage source;
    private final ArrayList<String> urlNames = new ArrayList<String>();
    private static final Pattern TAB_NEWLINE = Pattern.compile("[\n\t]");
    private ArrayList<Sentence> sentences = new ArrayList<Sentence>();
    private int sentenceCount;
    private int currentSentenceIndex;
    private boolean inDocument;

    ParseJsonStageTest(Stage source) {  // uses classic json.org parser
        this.source = source;
    }

    @Override
    Datum next() {
        // return from sentence buffer if not exhausted
        if (inDocument) {
            if (++currentSentenceIndex < sentenceCount) {
                return sentences.get(currentSentenceIndex);
            } else {
                inDocument = false;
                return EndDocument.INSTANCE;
            }
        }
        // otherwise try to refill sentence buffer
        final Datum next = source.next();
        if (next instanceof InputTextLine) {
            final String line = ((InputTextLine) next).line;
            try {
                final JSONObject rootNode = new JSONObject(line);
                readUrlNames(rootNode, urlNames);
                try {
                    sentences.clear();
                    readNerSentences(rootNode, sentences);
                    sentenceCount = sentences.size();
                    currentSentenceIndex = -1;
                    inDocument = true;
                    final JSONObject body = rootNode.getJSONObject("body");
                    final StartDocument doc = new StartDocument(rootNode.getString("doc_id"), urlNames);
                    doc.absUrl = rootNode.getString("abs_url");
                    doc.streamId = rootNode.has("stream_id") ? rootNode.getString("stream_id") : "";
                    doc.cleansedEscaped = body != null && body.has("cleansed") ? body.getString("cleansed") : "";
                    return doc;
                } catch (Exception e) {
                    return new StageException(e);
                }
            } catch (Exception e) {
                return new StageException(e);
            }
        } else {
            return next;
        }
    }

    private static void readUrlNames(final JSONObject rootNode, final ArrayList<String> urlNames) throws JSONException {
        urlNames.clear();
        if (rootNode.has("source_metadata")) {
            final JSONObject sourceMetadata = rootNode.getJSONObject("source_metadata");
            if (sourceMetadata.has("entities")) {
                final JSONArray entities = sourceMetadata.getJSONArray("entities");
                final int size = entities.length();
                for (int i = 0; i < size; ++i) {
                    final String urlName = entities.getJSONObject(i).getString("urlname");
                    if (urlName != null) {
                        urlNames.add(urlName);
                    }
                }
            }
        }
    }

    private static void readNerSentences(final JSONObject rootNode, final ArrayList<Sentence> sentences) throws Exception {
        if (rootNode.has("body")) {
            final JSONObject body = rootNode.getJSONObject("body");
            if (body.has("ner")) {
                final String ner = body.getString("ner");
                final byte[] bytes = StringEscapePythonUtils.unescapePython(ner);
                // making NerTokens from data in JSON can be speed optimized; simple 'split' for now
                final String[] parts = TAB_NEWLINE.split(new String(bytes));
                final int partCount = parts.length;
                final ArrayList<NerPhrase> tokens = new ArrayList<NerPhrase>();
                int lineNumber = 0;
                for (int offset = 0; offset < partCount; offset += 7) {
                    if (parts[offset].isEmpty()) {  // at end of sentence 2 newlines...
                        sentences.add(new Sentence(tokens.toArray(new NerPhrase[tokens.size()])));
                        tokens.clear();
                        ++offset;   // skip empty string at end of sentence
                        ++lineNumber;
                    }
                    final NerToken nerToken = new NerToken(Integer.parseInt(parts[offset]), // tokenId
                            parts[offset + 1],  // token
                            parts[offset + 2],  // lemma
                            parts[offset + 3],  // pos
                            parts[offset + 4],  // entity type
                            Integer.parseInt(parts[offset + 5]),    // start
                            Integer.parseInt(parts[offset + 6]),
                            lineNumber++);   // end
                    tokens.add(nerToken);
                }
            }
        }
    }
}
