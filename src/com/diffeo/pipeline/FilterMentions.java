package com.diffeo.pipeline;

final class FilterMentions extends Stage {
    private final Stage source;

    FilterMentions(Stage source) {
        this.source = source;
    }

    @Override
    Datum next() {
        Datum datum;
        while (isMentionWithoutCanopies(datum = source.next())) {
        }
        return datum;
    }

    private static boolean isMentionWithoutCanopies(final Datum datum) {
        return datum instanceof Mention && ((Mention) datum).canopies.isEmpty();
    }
}
