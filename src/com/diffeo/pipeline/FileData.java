package com.diffeo.pipeline;

final class FileData extends Datum {
    private final String fileName;
    private final byte[] data;

    public FileData(String fileName, byte[] data) {
        this.fileName = fileName;
        this.data = data;
    }

    public String getFileName() {
        return fileName;
    }

    public byte[] getData() {
        return data;
    }
}
