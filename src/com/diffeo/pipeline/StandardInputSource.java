package com.diffeo.pipeline;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

final class StandardInputSource extends Stage {
    private BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    @Override
    Datum next() {
        try {
            final String line = reader.readLine();
            if (line != null) {
                return new InputTextLine(line);
            } else {
                reader.close();
                reader = null;
                return EndOfData.INSTANCE;
            }
        } catch (IOException e) {
            return new StageException(e);
        }
    }
}
