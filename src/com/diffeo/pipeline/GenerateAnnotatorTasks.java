package com.diffeo.pipeline;

import com.fasterxml.jackson.core.JsonEncoding;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Copyright 2012 Diffeo, Inc. All Rights Reserved.
 * This software is the proprietary information of Diffeo, Inc.
 * Use is subject to license terms.
 *
 * @author Jacek R. Ambroziak
 */
final class GenerateAnnotatorTasks extends Stage {
    private final Stage source;
    private StartDocument document;
    private JsonGenerator jsonGen;
    private ArrayList<NerToken> tokens = new ArrayList<NerToken>();

    private HashMap<String, ArrayList<Mention>> groupByCanopy = new HashMap<String, ArrayList<Mention>>();
    private HashMap<Entity, ArrayList<Mention>> mentionsByEntity = new HashMap<Entity, ArrayList<Mention>>();

    GenerateAnnotatorTasks(Stage source, String docPath) {
        this.source = source;
        jsonGen = makeJsonGenerator(docPath);
    }

    private static JsonGenerator makeJsonGenerator(final String docPath) {
        try {
            final JsonFactory factory = new JsonFactory();
            if (docPath.equals("-")) {
                return factory.createJsonGenerator(System.out, JsonEncoding.UTF8);
            } else {
                return factory.createJsonGenerator(new File(docPath), JsonEncoding.UTF8);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private String classifyMentionByCanopy(final Mention mention) {
        final String canopy = mention.canopies.first(); // should be the only one
        ArrayList<Mention> list = groupByCanopy.get(canopy);
        if (list == null) {
            groupByCanopy.put(canopy, list = new ArrayList<Mention>(1));
        }
        list.add(mention);
        return canopy;
    }

    private void indexByMatchingEntities(final Mention mention) {
        if (mention.matchingEntities.isEmpty()) {
            //System.out.println(mention.name);
            classifyMentionByCanopy(mention);
        } else {
            for (final Entity entity : mention.matchingEntities) {
                ArrayList<Mention> mentions = mentionsByEntity.get(entity);
                if (mentions == null) {
                    mentionsByEntity.put(entity, mentions = new ArrayList<Mention>(1));
                    mentions.add(mention);
                } else if (mentions.contains(mention)) {
                } else {
                    mentions.add(mention);
                }
            }
        }
    }

    @Override
    Datum next() {
        try {
            Datum datum;
            while ((datum = source.next()) != EndOfData.INSTANCE) {
                if (datum instanceof Mention) {
                    indexByMatchingEntities((Mention) datum);   // accumulate Mentions
                } else if (datum instanceof StartDocument) {
                    setDocument((StartDocument) datum);         // set context Doc for the Mentions
                } else if (datum instanceof EndDocument) {
                    writeJson();    // write out JSON based on context and accumulated Mentions
                    document = null;
                }
            }
            jsonGen.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return EndOfData.INSTANCE;
    }

    private void writeJson() throws IOException {
        if (mentionsByEntity.size() > 0) {
            final JsonGenerator jg = jsonGen;
            for (final Entity entity : mentionsByEntity.keySet()) {
                final ArrayList<Mention> mentions = mentionsByEntity.get(entity);
                // add full recall Mentions
                //if (groupByCanopy.containsKey(entity.label)) {
                //    mentions.addAll(groupByCanopy.get(entity.label));
                //}
                jg.writeStartObject();
                jg.writeStringField("doc_id", document.docId);
                jg.writeStringField("stream_id", document.streamId);
                jg.writeStringField("urlname", entity.label);
                jg.writeStringField("abs_url", document.absUrl);
                jg.writeStringField("cleansed", document.cleansedEscaped);
                jg.writeNumberField("mention_cnt", mentions.size());
                {
                    jg.writeFieldName("mentions");
                    jg.writeStartArray();
                    for (final Mention mention : mentions) {
                        jg.writeStartObject();
                        jg.writeStringField("mention_id", mention.uuid.toString());
                        jg.writeStringField("window", mention.window());
                        {
                            jg.writeFieldName("tokens");
                            jg.writeStartArray();
                            tokens.clear(); // reuse this array
                            for (final NerToken nerToken : mention.loadTokens(tokens)) {
                                jg.writeStartObject();
                                jg.writeStringField("tok", nerToken.token);
                                jg.writeNumberField("start", nerToken.start);
                                jg.writeNumberField("end", nerToken.end);
                                jg.writeNumberField("line", nerToken.lineNumber);
                                jg.writeEndObject();
                            }
                            jg.writeEndArray();
                        }
                        jg.writeEndObject();
                    }
                    jg.writeEndArray();
                }
                jg.writeEndObject();
                jg.writeRaw('\n');  // lay out objects line by line
            }
        }
    }

    private void setDocument(final StartDocument document) {
        groupByCanopy.clear();
        mentionsByEntity.clear();
        this.document = document;
    }
}
