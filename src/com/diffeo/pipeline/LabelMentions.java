package com.diffeo.pipeline;

import java.io.IOException;
import java.util.TreeSet;

/**
 * Copyright 2012 Diffeo, Inc. All Rights Reserved.
 * This software is the proprietary information of Diffeo, Inc.
 * Use is subject to license terms.
 *
 * @author Jacek R. Ambroziak
 */
final class LabelMentions extends Stage {
    private final Stage source;
    private final EntityLexicon entityLexicon;

    LabelMentions(Stage source, EntityLexicon entityLexicon) throws IOException {
        this.source = source;
        this.entityLexicon = entityLexicon;
    }

    @Override
    Datum next() {
        final Datum datum = source.next();
        return datum instanceof Mention ? labelMention((Mention) datum) : datum;
    }

    private Datum labelMention(final Mention mention) {
        final TreeSet<Entity> entities = entityLexicon.phraseMatches(mention.name);    // 'name' is join(tokens, ' ')
        if (entities != null) { // (no need to check if non-empty; if it is there, it is non-empty)
            switch (entities.size()) {
                case 0:
                    return new StageException("empty entity set for " + mention.name);

                case 1:
                    mention.label = entities.first().label;
                    break;

                default:
                    return new StageException("multiple entities for " + mention.name);
            }
        }
        return mention;
    }
}
