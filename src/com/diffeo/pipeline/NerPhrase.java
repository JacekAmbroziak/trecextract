package com.diffeo.pipeline;

import java.util.ArrayList;

/**
 * Copyright 2012 Diffeo, Inc. All Rights Reserved.
 * This software is the proprietary information of Diffeo, Inc.
 * Use is subject to license terms.
 *
 * @author Jacek R. Ambroziak
 */
interface NerPhrase {
    String getEntityMentionType();

    StringBuilder appendTo(final StringBuilder sb);

    String text();

    StringBuilder appendAsText(StringBuilder sb);

    ArrayList<NerToken> append(ArrayList<NerToken> result);
}
