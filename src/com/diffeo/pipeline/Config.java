package com.diffeo.pipeline;

import java.io.File;

final class Config {
    final String[] args;

    Config(String[] args) {
        this.args = args;
    }

    Stage getSource() throws Exception {
        if (args.length == 0) {
            return new StandardInputSource();
        } else {
            final String inputArg = args[0];
            final File file = new File(inputArg);
            if (file.exists()) {
                if (file.isDirectory()) {
                    return new DirSource(inputArg);
                } else if (inputArg.endsWith(".tar") || inputArg.endsWith(".tar.gz")) {
                    return new TarSource(file);
                } else if (inputArg.endsWith(".gz")) {  // but not .tar.gz
                    return new SingleFileSource(file);
                } else {
                    throw new Exception("unknown input " + inputArg);
                }
            } else {
                throw new Exception(inputArg + " doesn't exist");
            }
        }
    }
}
