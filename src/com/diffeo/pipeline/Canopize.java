package com.diffeo.pipeline;

import java.io.IOException;
import java.util.ArrayList;
import java.util.TreeSet;

final class Canopize extends Stage {
    private final Stage source;
    private final EntityLexicon entityLexicon;
    private ArrayList<NerToken> tokenList = new ArrayList<NerToken>(); // auxiliary, reusable list

    Canopize(Stage source, EntityLexicon entityLexicon) throws IOException {
        this.source = source;
        this.entityLexicon = entityLexicon;
    }

    @Override
    Datum next() {
        final Datum datum = source.next();
        if (datum instanceof Mention) {
            addCanopies((Mention) datum);
        }
        return datum;
    }

    private void addCanopies(final Mention mention) {
        tokenList.clear();
        for (final NerToken token : mention.loadTokens(tokenList)) {
            final TreeSet<Entity> entities = entityLexicon.lowercaseTokenMatches(token.token.toLowerCase());
            if (entities != null) {
                for (Entity entDesc : entities) {
                    mention.canopies.add(entDesc.label);
                }
            }
        }
    }
}
