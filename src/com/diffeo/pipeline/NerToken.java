package com.diffeo.pipeline;

import java.util.ArrayList;

/**
 * Copyright 2012 Diffeo, Inc. All Rights Reserved.
 * This software is the proprietary information of Diffeo, Inc.
 * Use is subject to license terms.
 *
 * @author Jacek R. Ambroziak
 */
final class NerToken implements NerPhrase {
    final String entityMentionType;
    final int tokenId;
    final String token;
    final String lemma;
    final String pos;
    final int start;
    final int end;
    final int lineNumber;

    NerToken(int tokenId, String token, String lemma, String pos, String entityMentionType, int start, int end, int ln) {
        this.entityMentionType = entityMentionType.intern();    // will be checked for equality with ==
        this.tokenId = tokenId;
        this.token = token;
        this.lemma = lemma;
        this.pos = pos;
        this.start = start;
        this.end = end;
        this.lineNumber = ln;
    }

    @Override
    public String getEntityMentionType() {
        return entityMentionType;
    }

    @Override
    public StringBuilder appendTo(StringBuilder sb) {
        return sb.append(token);
    }

    @Override
    public String text() {
        return token;
    }

    @Override
    public StringBuilder appendAsText(StringBuilder sb) {
        return sb.append(token);
    }

    @Override
    public ArrayList<NerToken> append(ArrayList<NerToken> result) {
        result.add(this);
        return result;
    }

    @Override
    public String toString() {
        return String.valueOf(tokenId) + '\t' + entityMentionType + '\t' + token;
    }
}
