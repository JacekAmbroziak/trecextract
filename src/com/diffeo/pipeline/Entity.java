package com.diffeo.pipeline;

import java.util.List;

/**
 * Copyright 2012 Diffeo, Inc. All Rights Reserved.
 * This software is the proprietary information of Diffeo, Inc.
 * Use is subject to license terms.
 *
 * @author Jacek R. Ambroziak
 */
final class Entity {
    final String label; // e.g. "Paul_Graham_(computer_programmer)"
    List<String> tokens;    // e.g. {"Paul", "Graham"}
    String phrase;      // e.g. "Paul Graham"
    String[] redirects;

    Entity(String label) {
        this.label = label;
    }

    @Override
    public String toString() {
        return label + " => " + phrase;
    }

    @Override
    public boolean equals(final Object obj) {
        return obj instanceof Entity && label.equals(((Entity) obj).label);
    }

    @Override
    public int hashCode() {
        return label.hashCode();
    }
}
