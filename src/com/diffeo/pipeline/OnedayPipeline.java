package com.diffeo.pipeline;

/**
 * Copyright 2012 Diffeo, Inc. All Rights Reserved.
 * This software is the proprietary information of Diffeo, Inc.
 * Use is subject to license terms.
 *
 * @author Jacek R. Ambroziak
 */
public final class OnedayPipeline {
    public static void main(String[] args) {
        try {
            final Config config = new Config(args);
            final EntityLexicon entityLexicon = new EntityLexicon("config/entity-urlnames.json");

            final Stage source = config.getSource();
            final UnzipTextLines unzipper = new UnzipTextLines(source);
            final ParseJsonStage parseJsonStage = new ParseJsonStage(unzipper);
            final MakeTokenGroups groupTokens = new MakeTokenGroups(parseJsonStage);
            // create Mention data from appropriate NerPhrases and supply them with context info
            // such as surrounding sentences and "bags of words"
            final BuildMentions buildMentions = new BuildMentions(groupTokens);
            // supply Mentions with canopy labels
//            final HighPrecisionCanopize canopize = new HighPrecisionCanopize(buildMentions, entityLexicon);
            final Canopize canopize = new Canopize(buildMentions, entityLexicon);
            // drop Mentions with no canopy labels
            final FilterMentions filterMentions = new FilterMentions(canopize);
            final HighPrecisionEntityMatch match = new HighPrecisionEntityMatch(filterMentions, entityLexicon);
            // split Mentions with multiple canopy labels into multiple Mentions
            final SingleCanopyMentions singleCanopyMentions = new SingleCanopyMentions(match);
            //final GenerateAnnotatorTasks sink = new GenerateAnnotatorTasks(singleCanopyMentions, "tasks.json");
            final GenerateAnnotatorTasks sink = new GenerateAnnotatorTasks(singleCanopyMentions, "-");
            final long start = System.currentTimeMillis();
            Datum datum;
            while ((datum = sink.next()) != EndOfData.INSTANCE) {
                System.out.println(datum);
            }
            final long end = System.currentTimeMillis();
            System.err.println("DONE " + (end - start) + " msec");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
