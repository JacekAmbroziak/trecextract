package com.diffeo.pipeline;

/**
 * Copyright 2012 Diffeo, Inc. All Rights Reserved.
 * This software is the proprietary information of Diffeo, Inc.
 * Use is subject to license terms.
 *
 * @author Jacek R. Ambroziak
 */
final class StageException extends Datum {
    final Throwable throwable;

    StageException(Throwable throwable) {
        this.throwable = throwable;
    }

    StageException(String s) {
        this(new Exception(s));
    }
}
