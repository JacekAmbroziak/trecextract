package com.diffeo.pipeline;

import java.util.ArrayList;
import java.util.List;

/**
 * Date: 5/8/12
 * Time: 2:39 PM
 */
final class BuildMentions extends Stage {
    private final Stage source;
    private StartDocument document;
    private ArrayList<Sentence> sentences;
    private ArrayList<Mention> mentions;
    private int mentionCount;
    private int currentMention;

    BuildMentions(Stage source) {
        this.source = source;
        currentMention = -1;
    }

    @Override
    Datum next() {
        if (mentionCount > 0) {
            if (++currentMention < mentionCount) {
                return mentions.get(currentMention);
            } else {
                mentions = null;
                mentionCount = 0;
                return EndDocument.INSTANCE;
            }
        }


        final Datum terminator = readDocumentSentences();
        if (terminator == EndOfData.INSTANCE) {
            return terminator;
        }

        mentions = findMentions();
        mentionCount = mentions.size();
        currentMention = -1;

        return document;
    }

    private ArrayList<Mention> findMentions() {
        final ArrayList<Mention> mentions = new ArrayList<Mention>();
        final int sentenceCount = sentences.size();
        final ArrayList<String> bow = new ArrayList<String>();  // "bag of words" reusable list
        for (int i = 0; i < sentenceCount; i++) {
            final Sentence sentence = sentences.get(i);
            final NerPhrase[] phrasesOfInterest = sentence.getEntitiesOfInterest();
            final ArrayList<NerPhrase> allEntities = sentence.getAllEntities();
            final int entityCount = allEntities.size();
            // now only considering "phrases of interest" i.e. PERSONs and ORGANIZATIONs
            for (final NerPhrase phrase : phrasesOfInterest) {
                final Mention mention = new Mention(phrase.text());
                mention.phrase = phrase;
                bow.clear();    // reuse this list

                if (i > 0) {    // try previous sentence
                    final ArrayList<NerPhrase> previous = sentences.get(i - 1).getAllEntities();
                    final int length = previous.size();
                    addPhrases(previous.subList(Math.max(0, length - 3), length), bow);
                    mention.prevSentence = sentences.get(i - 1);
                }
                // find our entity among all entities in this sentence
                final int index = allEntities.indexOf(phrase);
                if (index < 0) {
                    throw new Error("entity not found " + phrase);
                }
                // add at most 3 preceding
                addPhrases(allEntities.subList(Math.max(0, index - 3), index), bow);
                // add at most 3 following
                addPhrases(allEntities.subList(index, Math.min(index + 3, entityCount)), bow);

                mention.sentence = sentence;
                if (i + 1 < sentenceCount) {    // try next sentence
                    final ArrayList<NerPhrase> next = sentences.get(i + 1).getAllEntities();
                    final int length = next.size();
                    addPhrases(next.subList(0, Math.min(3, length)), bow);
                    mention.nextSentence = sentences.get(i + 1);
                }
                mention.setBow(bow);
                mentions.add(mention);
            }
        }
        return mentions;
    }

    private static void addPhrases(final List<NerPhrase> list, final List<String> result) {
        for (NerPhrase nerPhrase : list) {
            result.add(nerPhrase.text());
        }
    }

    // returns datum that ended the sequence of sentences
    private Datum readDocumentSentences() {
        sentences = new ArrayList<Sentence>();
        document = null;
        for (; ; ) {
            final Datum datum = source.next();
            if (datum == EndOfData.INSTANCE) {
                return datum;
            } else if (datum instanceof StartDocument) {
                document = (StartDocument) datum;
                break;
            }
        }
        for (; ; ) {
            final Datum datum = source.next();
            if (datum instanceof Sentence) {
                sentences.add((Sentence) datum);
            } else if (datum instanceof EndDocument) {
                return datum;
            }
        }
    }
}
