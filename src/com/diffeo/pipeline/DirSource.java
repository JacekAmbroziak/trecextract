package com.diffeo.pipeline;

import java.io.File;
import java.io.FilenameFilter;

/**
 * User: jacek
 * Date: 5/5/12
 * Time: 12:52 PM
 *
 * @author Jacek R. Ambroziak
 */
final class DirSource extends Stage {
    private final File dir;
    private final String[] fileNames;
    private int current;

    DirSource(File dir) {
        this.dir = dir;
        fileNames = dir.list(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.endsWith(".gz");
            }
        });
        current = -1;   // before first
    }

    DirSource(String dirPath) {
        this(new File(dirPath));
    }

    boolean isNonEmptyDir() {
        return dir != null && dir.isDirectory() && dir.list().length > 0;
    }

    Datum next() {
        if (++current < fileNames.length) {
            System.err.println("DirSource: " + fileNames[current]);
            return new InputFile(new File(dir, fileNames[current]));
        } else {
            return EndOfData.INSTANCE;
        }
    }
}
