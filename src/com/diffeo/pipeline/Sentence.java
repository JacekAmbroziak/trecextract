package com.diffeo.pipeline;

import java.util.ArrayList;

/**
 * Copyright 2012 Diffeo, Inc. All Rights Reserved.
 * This software is the proprietary information of Diffeo, Inc.
 * Use is subject to license terms.
 *
 * @author Jacek R. Ambroziak
 */
final class Sentence extends Datum {
    private static final NerPhrase[] EMPTY_PHRASE = new NerPhrase[0];
    private static final String OTHER = "O".intern();
    private static final String PERSON = "PERSON".intern();
    private static final String ORGANIZATION = "ORGANIZATION".intern();

    final NerPhrase[] phrases;
    private NerPhrase[] entitiesOfInterest;
    private ArrayList<NerPhrase> allEntities;   // non-"OTHER" entities

    Sentence(NerPhrase[] phrases) {
        this.phrases = phrases;
    }

    NerPhrase[] getEntitiesOfInterest() {
        return entitiesOfInterest != null ? entitiesOfInterest : (entitiesOfInterest = ofInterest(phrases, allEntities = new ArrayList<NerPhrase>()));
    }

    ArrayList<NerPhrase> getAllEntities() {
        if (allEntities == null) {
            entitiesOfInterest = ofInterest(phrases, allEntities = new ArrayList<NerPhrase>());
        }
        return allEntities;
    }

    private static NerPhrase[] ofInterest(final NerPhrase[] phrases, final ArrayList<NerPhrase> entities) {
        final ArrayList<NerPhrase> result = new ArrayList<NerPhrase>();
        for (final NerPhrase phrase : phrases) {
            final String type = phrase.getEntityMentionType();
            if (type == OTHER) {    // == used on interned Strings instead of 'equals()'
                // ignore; this test comes first as OTHER is the most frequent type
            } else if (type == PERSON || type == ORGANIZATION) {
                result.add(phrase);
                entities.add(phrase);
            } else {
                entities.add(phrase);   // non-OTHER
            }
        }
        final int size = result.size();
        return size == 0 ? EMPTY_PHRASE : result.toArray(new NerPhrase[size]);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder(256);
        for (final NerPhrase phrase : phrases) {
            phrase.appendTo(sb).append(' ');
        }
        return sb.toString();
    }

    StringBuilder appendAsText(final StringBuilder sb) {
        for (final NerPhrase phrase : phrases) {
            phrase.appendAsText(sb).append(' ');
        }
//        sb.setLength(sb.length() - 1);
        return sb;
    }

    StringBuilder appendAsTextWithSpan(final StringBuilder sb, final NerPhrase phrase) {
        final NerPhrase[] phrases = this.phrases;
        final int count = phrases.length;
        int i = 0;
        while (i < count) {
            final NerPhrase ph = phrases[i++];
            if (ph == phrase) {
                ph.appendAsText(sb.append("<span class=\"m\">")).append("</span>").append(' ');
                break;
            } else {
                ph.appendAsText(sb).append(' ');
            }
        }
        while (i < count) {
            phrases[i++].appendAsText(sb).append(' ');
        }
        return sb;
    }
}
