package com.diffeo.pipeline;

import java.util.ArrayList;

/**
 * Copyright 2012 Diffeo, Inc. All Rights Reserved.
 * This software is the proprietary information of Diffeo, Inc.
 * Use is subject to license terms.
 *
 * @author Jacek R. Ambroziak
 */
final class NerTokenGroup implements NerPhrase {
    final String entityMentionType;
    final NerPhrase[] phrases;
    private String text;    // cached String representation

    NerTokenGroup(String entityMentionType, NerPhrase[] phrases) {
        this.entityMentionType = entityMentionType; // inherited interned
        this.phrases = phrases;
    }

    @Override
    public String getEntityMentionType() {
        return entityMentionType;
    }

    @Override
    public StringBuilder appendTo(final StringBuilder sb) {
        sb.append('[');
        for (final NerPhrase token : phrases) {
            token.appendTo(sb).append(' ');
        }
        sb.setCharAt(sb.length() - 1, ']');
        return sb;
    }

    @Override
    public String text() {
        if (text != null) {
            return text;
        } else {
            final NerPhrase[] phrases = this.phrases;
            final StringBuilder sb = new StringBuilder(64).append(phrases[0].text());
            for (int i = 1; i < phrases.length; ++i) {
                sb.append(' ').append(phrases[i].text());
            }
            return text = sb.toString();
        }
    }

    @Override
    public StringBuilder appendAsText(final StringBuilder sb) {
        int last = phrases.length - 1;
        for (int i = 0; i < last; i++) {
            phrases[i].appendAsText(sb).append(' ');
        }
        return phrases[last].appendAsText(sb);
    }

    @Override
    public ArrayList<NerToken> append(ArrayList<NerToken> result) {
        for (NerPhrase token : phrases) {
            token.append(result);
        }
        return result;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder(64);
        for (NerPhrase token : phrases) {
            sb.append(token).append(' ');
        }
        return sb.toString();
    }
}
